import pyspark
from delta import *
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.functions import *


def get_spark_session(app_name: str) -> SparkSession:
    builder = pyspark.sql.SparkSession.builder.appName(app_name) \
        .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
        .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")

    return configure_spark_with_delta_pip(builder).getOrCreate()


def create_delta_table(data_frame: DataFrame, location: str, format: str) -> None:
    data_frame.write.format(format).save(location)


def read_delta_table(spark_session: SparkSession, location: str) -> DataFrame:
    return spark_session.read.format("delta").load(location)


def update_delta_table(data_frame: DataFrame, mode: str, format: str, location: str) -> None:
    """ updating a delta table with  mode """
    data_frame.write.format(format).mode(mode).save(location)


def conditional_update(delta_table: DeltaTable, column: str, *expressions: Column) -> None:
    """ conditional update on table"""
    delta_table.update(condition=expressions[0], set={column: expressions[1]})


def conditional_delete(spark_session: SparkSession, location: str, condition: str) -> None:
    """ conditional delete on delta table"""
    delta_table = DeltaTable.forPath(spark_session, location)
    delta_table.delete(condition=expr(condition))


def merge_or_upsert_on_delta_table(df: DataFrame, spark_session: SparkSession, location: str, condition: str) -> None:
    """ perform upsert or merge operation on a table"""
    delta_table = DeltaTable.forPath(spark_session, location)
    delta_table.alias("old_data") \
        .merge(df.alias("new_data"), condition) \
        .whenMatchedUpdate(set={"id":  col("new_data.id")}) \
        .whenNotMatchedInsert(values={"id": col("new_data.id")}) \
        .execute()


def read_older_version_delta_table(spark_session: SparkSession, location: str, version: int) -> DataFrame:
    """ reading particular version of table using versionAsOf option"""
    return spark_session.read.format("delta").option("versionAsOf", version).load(location)


def write_stream_of_data_delta_table(spark_session: SparkSession, checkpointLocation: str, table_location: str):
    """ structured streaming, by default stream runs in append mode , which append data to the table"""
    streaming_df = spark_session.readStream.format("rate").load()
    return streaming_df.selectExpr(
        "value as id").writeStream.format("delta").option("checkpointLocation", checkpointLocation).start(table_location)


def read_stream_changes_delta_table(spark_session: SparkSession, location: str):
    return spark_session.readStream.format("delta").load(location).writeStream.format("console").start()
