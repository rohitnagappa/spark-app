from utils import get_spark_session, read_stream_changes_delta_table
from pyspark.sql.functions import *
from pyspark.sql.types import *

# creating a spark session
spark_session = get_spark_session("rohit-nagappa")

schema = StructType([
    StructField("id", IntegerType(), False),
    StructField("name", StringType(), True)
])

df = spark_session.createDataFrame([
    (1, "rohit")
], schema=schema)

df.write.format("delta").saveAsTable("default.people12m")
