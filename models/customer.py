class Customer(object):

    def __init__(self, name: str, age: int, email: str, phone_no: int) -> None:
        self.name = name
        self.age = age
        self.email = email
        self.phone_no = phone_no
